package com.example.m0d1x.universityresturant.Activities.UserPanel;

import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.m0d1x.universityresturant.Common.Common;
import com.example.m0d1x.universityresturant.R;

public class UserPanelActivity extends AppCompatActivity {


    private TextView usr_name, usr_bday, usr_email, usr_gender, usr_membership, usr_comments, usr_Favs, usr_Addresses;

    private CircleImageView usr_img;

    private ImageView usr_membertype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_panel);


        if (Common.CurrentUser != null) {
            setupViews();
        }

    }

    private void setupViews() {
        usr_name = findViewById(R.id.tv_contact_name);
        usr_bday = findViewById(R.id.tv_contact_birthday);
        usr_email = findViewById(R.id.tv_contact_email);
        usr_gender = findViewById(R.id.tv_contact_gender);
        usr_membership = findViewById(R.id.tv_contact_Membership);
        usr_comments = findViewById(R.id.hint_contact_comments);
        usr_Favs = findViewById(R.id.tv_user_favourites);
        usr_Addresses = findViewById(R.id.tv_shippingAddresses);
        usr_membertype = findViewById(R.id.usr_img_type);
        usr_img = findViewById(R.id.usr_img);

        usr_name.setText(Common.CurrentUser.getName());

        usr_email.setText(Common.CurrentUser.getEmail());


    }

}
