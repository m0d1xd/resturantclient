package com.example.m0d1x.universityresturant.Activities.Authontication;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.m0d1x.universityresturant.Activities.Home.HomeActivity;
import com.example.m0d1x.universityresturant.Common.Common;
import com.example.m0d1x.universityresturant.R;
import com.example.m0d1x.universityresturant.model.User;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class SignUpActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser firebaseUser;

    private GoogleSignInClient mGoogleSignInClient;

    private EditText et_password, et_email, et_name;

    private Button btn_signUp;
    final String TAG = "SignUpActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        final ProgressDialog mDialog = new ProgressDialog(SignUpActivity.this);

        mDialog.setMessage("Please Wait ..");
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    FirebaseDatabase db = FirebaseDatabase.getInstance();
                    DatabaseReference user_info = db.getReference("Users");
                    user_info.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            mDialog.dismiss();
                            try {
                                Common.CurrentUser = dataSnapshot.child(firebaseUser.getUid()).getValue(User.class);
                                System.out.println(Common.CurrentUser.toString());
                                Intent i = new Intent(SignUpActivity.this, HomeActivity.class);
                                startActivity(i);

                            } catch (Exception e) {
                                System.out.println("Line 61 MainActivity" + e.toString());
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    mDialog.dismiss();
                }
                // ...
            }
        };

        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_name = findViewById(R.id.et_name);
        btn_signUp = findViewById(R.id.btn_SignUp);
        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidation(et_email, et_password, et_name);

                final String Email = et_email.getText().toString();
                final String Password = et_password.getText().toString();
                final String Name = et_name.getText().toString();
                mAuth.createUserWithEmailAndPassword(Email, Password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            String uid = null;
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                                uid = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
                            }
                            DatabaseReference curent_user_db = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
                            User user = new User();
                            user.setEmail(Email);
                            user.setName(Name);
                            user.setUid(uid);
                            user.setIsStaff("false");
                            user.setPassword(Password);
                            curent_user_db.setValue(user);
                            Common.CurrentUser = user;
                            Toast.makeText(getApplicationContext(), "Account Created Succesfully", Toast.LENGTH_SHORT).show();
                            startApp();
                        } else {
                            Toast.makeText(getApplicationContext(), "Error Creating Account", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
            }
        });
    }

    private void startApp() {
        Intent i = new Intent(SignUpActivity.this, HomeActivity.class);
        startActivity(i);
    }

    //Checking Email And password Validation
    public void checkValidation(EditText editTextEmail, EditText editTextPassword, EditText editFullName) {


        String email, password, name;
        //Checking Email Validation
        email = editTextEmail.getText().toString();
        if (email.isEmpty()) {
            editTextEmail.setError(getString(R.string.input_error_email));
            editTextEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmail.setError(getString(R.string.input_error_email_invalid));
            editTextEmail.requestFocus();
            return;
        }

        //Checking Password Validation
        password = editTextPassword.getText().toString();
        if (password.isEmpty()) {
            editTextPassword.setError(getString(R.string.input_error_password));
            editTextPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            editTextPassword.setError(getString(R.string.input_error_password_length));
            editTextPassword.requestFocus();
            return;
        }
        //Checking Name Is not Empty
        name = editFullName.getText().toString();
        if (name.isEmpty()) {
            editTextEmail.setError(getString(R.string.input_error_name));
            editTextEmail.requestFocus();
            return;
        }
    }

}
