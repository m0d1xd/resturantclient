package com.example.m0d1x.universityresturant.Activities.Home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.m0d1x.universityresturant.Common.Common;
import com.example.m0d1x.universityresturant.Database.Database;
import com.example.m0d1x.universityresturant.R;
import com.example.m0d1x.universityresturant.model.Food;
import com.example.m0d1x.universityresturant.model.Order;
import com.example.m0d1x.universityresturant.model.Rating;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

public class FoodDetails extends AppCompatActivity implements RatingDialogListener {
    TextView tv_itemName, tv_itemPrice, tv_itemDescription;
    ImageView img_item;
    CollapsingToolbarLayout collapsingToolbarLayout;
    FloatingActionButton btn_Rating;
    ElegantNumberButton btn_quantity;
    String foodId = "";
    CounterFab btn_toCart;
    FirebaseDatabase database;
    DatabaseReference items, ratingTable;
    Food CurrentFood;
    RatingBar ratingBar;
    Button ShowComments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_details_newlayout);
        //Init Firebase

        database = FirebaseDatabase.getInstance();
        items = database.getReference("Foods");
        ratingTable = database.getReference("Ratings");

        //Init Views
        btn_Rating = findViewById(R.id.btn_rating);
        ratingBar = findViewById(R.id.ratingBar);
        ShowComments = findViewById(R.id.btn_showComments);
        btn_quantity = findViewById(R.id.btn_quantity);

        btn_toCart = findViewById(R.id.btn_toCart);

        btn_toCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Database(getBaseContext()).addToCart(new Order(
                        foodId,
                        CurrentFood.getName(),
                        btn_quantity.getNumber(),
                        CurrentFood.getPrice(),
                        CurrentFood.getDiscount(),
                        CurrentFood.getImage()
                ));

                Toast.makeText(getApplicationContext(), "Added To Cart", Toast.LENGTH_SHORT).show();
            }
        });

        ShowComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FoodDetails.this, CommentsActivity.class);
                intent.putExtra(Common.INTENT_FOOD_ID, foodId);
                startActivity(intent);

            }
        });

        btn_quantity.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                tv_itemPrice.setText("" + Integer.parseInt(CurrentFood.getPrice()) * newValue);
            }
        });

        btn_toCart.setCount(new Database(this).getCartCount());
        btn_Rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowRatingDialog();
            }
        });
        tv_itemName = findViewById(R.id.tv_food_name);
        tv_itemPrice = findViewById(R.id.tv_foodPrice);
        tv_itemDescription = findViewById(R.id.tv_food_Description);
        img_item = findViewById(R.id.img_food);

        collapsingToolbarLayout = findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsingToolbar);

        //Getting Food id from previous intent
        if (getIntent() != null) {
            foodId = getIntent().getStringExtra("foodID");
        }
        if (!foodId.isEmpty()) {
            Log.d("FoodID", "onCreate: foodId " + foodId);
            getFoodDetails(foodId);
            getFoodRating(foodId);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        btn_toCart.setCount(new Database(this).getCartCount());

    }

    private void ShowRatingDialog() {

        new AppRatingDialog.Builder().setTitle("Rate This Food")
                .setDescription("Please Tell us your opinion About this food")
                .setTitleTextColor(R.color.colorPrimary)
                .setDescriptionTextColor(R.color.colorPrimary)
                .setHint("Yor comment here ")
                .setCommentTextColor(android.R.color.white)
                .setWindowAnimation(R.style.RatingDialogFadeAnim)
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setDefaultRating(0)
                .setNumberOfStars(5)
                .create(FoodDetails.this)
                .show();
    }

    private void getFoodDetails(String FoodId) {

        items.child(FoodId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                CurrentFood = dataSnapshot.getValue(Food.class);
                Picasso.with(getBaseContext()).load(CurrentFood.getImage()).into(img_item);
                collapsingToolbarLayout.setTitle(CurrentFood.getName());
                tv_itemPrice.setText(CurrentFood.getPrice());
                tv_itemName.setText(CurrentFood.getName());
                tv_itemDescription.setText(CurrentFood.getDescription());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onNegativeButtonClicked() {

    }

    @Override
    public void onPositiveButtonClicked(int value, String comment) {

        final Rating rating = new Rating(foodId, String.valueOf(value), comment, Common.CurrentUser.getName(), Common.CurrentUser.getUid());
        ratingTable.push().setValue(rating).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(FoodDetails.this, "Thank You for your Ratting", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getFoodRating(final String x) {
        ratingTable.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int count = 0, sum = 0;
                for (DataSnapshot post : dataSnapshot.getChildren()) {
                    Rating rate = post.getValue(Rating.class);
                    if (x.equals(rate.getFoodId())) {
                        sum += Integer.parseInt(rate.getRateValue());
                        count++;
                        if (count != 0) {
                            float avg = sum / count;
                            ratingBar.setRating(avg);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


}
