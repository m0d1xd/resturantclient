package com.example.m0d1x.universityresturant.model;

import com.firebase.geofire.GeoFire;

public class Address {
    private String city, streetAddress, homeNumber, moreInfo;
    private GeoFire coordinates;

    public Address() {
    }

    public String getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(String moreInfo) {
        this.moreInfo = moreInfo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public GeoFire getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(GeoFire coordinates) {
        this.coordinates = coordinates;
    }
}
