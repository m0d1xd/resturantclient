package com.example.m0d1x.universityresturant.Remote;

import com.example.m0d1x.universityresturant.model.NotificationModels.FCMResponse;
import com.example.m0d1x.universityresturant.model.NotificationModels.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIServices {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAoAzul-Y:APA91bEEw61DVKFgEjzFvee1dM0_d4O3MT7B7qKj425ZCbtOarj24sgLZYalNjuV2jKE4R_zAH09lTjFFSH1zLRtjEjcdoPveF-jVchbQtc8Mqprg1FvcxJjn826ut5_FMDIQwwRs66cUYuyFw6h3RBsu0s7Qhs4Ow"
            }
    )


    @POST("fcm/send")
    Call<FCMResponse> sendNotification(@Body Sender body);

}
