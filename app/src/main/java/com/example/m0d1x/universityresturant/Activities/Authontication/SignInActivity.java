package com.example.m0d1x.universityresturant.Activities.Authontication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.m0d1x.universityresturant.Activities.Home.HomeActivity;
import com.example.m0d1x.universityresturant.Common.Common;
import com.example.m0d1x.universityresturant.R;
import com.example.m0d1x.universityresturant.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SignInActivity extends AppCompatActivity {
    private EditText et_email, et_password;
    private Button btn_login;
    private FirebaseUser firebaseUser;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    final String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        et_email = (AppCompatEditText) findViewById(R.id.et_email);
        et_password = (AppCompatEditText) findViewById(R.id.et_password);
        btn_login = findViewById(R.id.btn_SignIn);
        final ProgressDialog mDialog = new ProgressDialog(SignInActivity.this);
        mDialog.setMessage("Please Wait  ...");
        //Firebase init

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    FirebaseDatabase db = FirebaseDatabase.getInstance();
                    DatabaseReference user_info = db.getReference("Users");
                    user_info.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            try {
                                Common.CurrentUser = dataSnapshot.child(firebaseUser.getUid()).getValue(User.class);
                                System.out.println(Common.CurrentUser.toString());
                                //TODO EDIT TO LOG TO HOME
                                Intent i = new Intent(SignInActivity.this, HomeActivity.class);
                                startActivity(i);
                            } catch (Exception e) {
                                System.out.println("Line 61 MainActivity" + e.toString());
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    mDialog.dismiss();
                }
                // ...
            }
        };

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.show();
                final String email = et_email.getText().toString();
                final String password = et_password.getText().toString();
                checkValidation(et_email, et_password);
                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(
                        new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(getApplicationContext(),
                                            "Login Error please Check Email or password",
                                            Toast.LENGTH_LONG).show();
                                    Log.d(TAG, "onComplete: " + task.getException());
                                } else {
                                    firebaseUser = mAuth.getCurrentUser();
                                    DatabaseReference db = FirebaseDatabase.getInstance().getReference("Users");
                                    db.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        //  Common.CurrentUser = firebaseUser.getUid();
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            Common.CurrentUser = dataSnapshot.child(firebaseUser.getUid()).getValue(User.class);
                                            mDialog.dismiss();
                                            Intent i = new Intent(SignInActivity.this, HomeActivity.class);
                                            startActivity(i);
                                            finish();
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            mDialog.dismiss();
                                        }
                                    });
                                }
                                mDialog.dismiss();
                            }
                        });
            }
        });

    }


    //Checking Email And password Validation
    public void checkValidation(EditText editTextEmail, EditText editTextPassword) {
        String email, password;
        email = editTextEmail.getText().toString();
        if (email.isEmpty()) {
            editTextEmail.setError(getString(R.string.input_error_email));
            editTextEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmail.setError(getString(R.string.input_error_email_invalid));
            editTextEmail.requestFocus();
            return;
        }
        password = editTextPassword.getText().toString();
        if (password.isEmpty()) {
            editTextPassword.setError(getString(R.string.input_error_password));
            editTextPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            editTextPassword.setError(getString(R.string.input_error_password_length));
            editTextPassword.requestFocus();
            return;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthListener);
    }
}
