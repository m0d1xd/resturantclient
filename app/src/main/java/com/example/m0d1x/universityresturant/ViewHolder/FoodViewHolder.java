package com.example.m0d1x.universityresturant.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.m0d1x.universityresturant.Interface.ItemClickListener;
import com.example.m0d1x.universityresturant.R;

import androidx.recyclerview.widget.RecyclerView;

public class FoodViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView tv_food_name, tv_food_price;
    public ImageView img_food, img_fav, img_addToCart;

    private ItemClickListener itemClickListener;


    public FoodViewHolder(View itemView) {
        super(itemView);

        tv_food_name = itemView.findViewById(R.id.tv_food_name);
        tv_food_price = itemView.findViewById(R.id.tv_food_price);
        img_food = itemView.findViewById(R.id.img_food);
        img_fav = itemView.findViewById(R.id.fav);
        img_addToCart = itemView.findViewById(R.id.addToCart);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }

}
