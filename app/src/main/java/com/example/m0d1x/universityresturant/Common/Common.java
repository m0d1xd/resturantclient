package com.example.m0d1x.universityresturant.Common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.example.m0d1x.universityresturant.Remote.APIServices;
import com.example.m0d1x.universityresturant.Remote.IGoogleService;
import com.example.m0d1x.universityresturant.Remote.RetrofitClient;
import com.example.m0d1x.universityresturant.model.Request;
import com.example.m0d1x.universityresturant.model.User;


public class Common {
    public static User CurrentUser;

    public static Request CurrentRequest;

    public final static String DELETE = "Delete";

    public final static String INTENT_FOOD_ID = "FoodID";

    public static final String BASE_URL = "https://fcm.googleapis.com/";

    public static final String GOOGLE_API_URL = "https://maps.googleapis.com/";

    public static APIServices getFCMService() {
        return RetrofitClient.getClient(BASE_URL).create(APIServices.class);
    }


    public static IGoogleService getGoogleMapAPI() {
        return RetrofitClient.getGoogleClient(GOOGLE_API_URL).create(IGoogleService.class);
    }

    public static String convertCodeToString(String code) {
        switch (code) {
            case "0":
                return "Placed";
            case "1":
                return "On My Way";
            case "2":
                return "Shipped";
            default:
                return "UnKnown";
        }
    }

    //Checking Internet Connection
    public static Boolean IsConnectedToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static String getDate(String time) {
        long convert = Long.parseLong(time);

//        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
//        calendar.setTimeInMillis(convert);
//
//        DateFormat df = new android.text.format.DateFormat();
//        df.format("yyyy-MM-dd hh:mm", calendar);
        return Long.valueOf(convert).toString();

    }
}
