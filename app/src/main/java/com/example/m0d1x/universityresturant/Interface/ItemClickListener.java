package com.example.m0d1x.universityresturant.Interface;


import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
