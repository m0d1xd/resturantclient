package com.example.m0d1x.universityresturant.model;

public class Order {

    private String ProductID, ProductName, Quantity, Price, Discount, Image;
    private int ID;

    public Order() {
    }


    public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        Image = Image;
    }

    public Order(String productID, String productName, String quantity, String price, String discount, String Image) {
        ProductID = productID;
        ProductName = productName;
        Quantity = quantity;
        Price = price;
        Discount = discount;
        this.Image = Image;
    }

    public Order(int iD, String productID, String productName, String quantity, String price, String discount, String image) {
        ProductID = productID;
        ProductName = productName;
        Quantity = quantity;
        Price = price;
        Discount = discount;
        ID = iD;
        Image = image;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }
}
