package com.example.m0d1x.universityresturant.Activities.UserPanel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.example.m0d1x.universityresturant.Database.Database;
import com.example.m0d1x.universityresturant.Interface.ItemClickListener;
import com.example.m0d1x.universityresturant.R;
import com.example.m0d1x.universityresturant.ViewHolder.FavouritesAdapter;
import com.example.m0d1x.universityresturant.model.Favourites;

import java.util.List;

public class MyFavorites extends AppCompatActivity
        implements View.OnClickListener {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private FavouritesAdapter adapter;
    private ItemClickListener itemClickListener;

    private List<Favourites> favouritesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favoutis);

        new Database(this);

        recyclerView = findViewById(R.id.rv_myFavourites);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        loadFavourites();

    }

    private void loadFavourites() {

        favouritesList = new Database(this).getFavourites();

        FavouritesAdapter adapter = new FavouritesAdapter(favouritesList, this);

        adapter.notifyDataSetChanged();

        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {

    }

}
