package com.example.m0d1x.universityresturant.model;

public class Rating {
    private String foodId, rateValue, comment, userName, uid, datetime;

    public Rating() {
    }

    public Rating(String foodId, String rateValue, String comment) {
        this.foodId = foodId;
        this.rateValue = rateValue;
        this.comment = comment;
    }


    public Rating(String foodId, String rateValue, String comment, String userName, String uid) {
        this.foodId = foodId;
        this.rateValue = rateValue;
        this.comment = comment;
        this.userName = userName;
        this.uid = uid;
    }

    public Rating(String foodId, String rateValue, String comment, String userName, String uid, String datetime) {
        this.foodId = foodId;
        this.rateValue = rateValue;
        this.comment = comment;
        this.userName = userName;
        this.uid = uid;
        this.datetime = datetime;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getFoodId() {
        return foodId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setFoodId(String foodId) {
        this.foodId = foodId;
    }

    public String getRateValue() {
        return rateValue;
    }

    public void setRateValue(String rateValue) {
        this.rateValue = rateValue;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "foodId='" + foodId + '\'' +
                ", rateValue='" + rateValue + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
