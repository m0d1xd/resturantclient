package com.example.m0d1x.universityresturant.model.NotificationModels;

import com.google.gson.annotations.SerializedName;

public class Notification {
    @SerializedName("body")
    public String body;

    @SerializedName("title")
    public String title;

    public Notification(String body, String title) {
        this.body = body;
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "body='" + body + '\'' +
                ", title='" + title + '\'' +
                '}';
    }


}
