package com.example.m0d1x.universityresturant;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import info.hoang8f.widget.FButton;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.m0d1x.universityresturant.Activities.Authontication.SignInActivity;
import com.example.m0d1x.universityresturant.Activities.Authontication.SignUpActivity;
import com.example.m0d1x.universityresturant.Activities.Home.HomeActivity;
import com.example.m0d1x.universityresturant.Common.Common;
import com.example.m0d1x.universityresturant.model.NotificationModels.Token;
import com.example.m0d1x.universityresturant.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

public class MainActivity extends AppCompatActivity {


    private Button btn_signin, btn_signup;
    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser firebaseUser;
    final String TAG = "MainActivity;";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btn_signin = findViewById(R.id.btn_SignIn);
        btn_signup = findViewById(R.id.btn_SignUp);
        //Init Firebase
        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        final ProgressDialog mDialog = new ProgressDialog(MainActivity.this);

        mDialog.setMessage("Please Wait ..");

        mDialog.show();
        if (Common.IsConnectedToInternet(getApplicationContext())) {
            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    firebaseUser = firebaseAuth.getCurrentUser();
                    if (firebaseUser != null) {
                        DatabaseReference user_info = mFirebaseDatabase.getReference("Users");
                        user_info.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                mDialog.dismiss();
                                try {
                                    Common.CurrentUser = dataSnapshot.child(firebaseUser.getUid()).getValue(User.class);
                                    UpdateToken(FirebaseInstanceId.getInstance().getToken());
                                    Intent i = new Intent(MainActivity.this, HomeActivity.class);
                                    startActivity(i);
                                    finish();
                                } catch (Exception e) {
                                    System.out.println("Line 61 MainActivity" + e.toString());
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                    } else {
                        // User is signed out
                        Log.d(TAG, "onAuthStateChanged:signed_out");
                        mDialog.dismiss();
                    }
                    // ...
                }
            };
        } else {

            mDialog.dismiss();
            Toast.makeText(MainActivity.this, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();

        }

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.IsConnectedToInternet(getApplicationContext())) {
                    Intent intent = new Intent(MainActivity.this, SignInActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    mDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();

                }

            }
        });
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.IsConnectedToInternet(getApplicationContext())) {

                    Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

    }

    //Get Token When First Log in
    private void UpdateToken(String token) {
        DatabaseReference tokens = mFirebaseDatabase.getReference("Tokens");
        Token tokenData = new Token(token, false);
        tokens.child(Common.CurrentUser.getUid()).setValue(tokenData);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (Common.IsConnectedToInternet(getApplicationContext()))
            mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (Common.IsConnectedToInternet(getApplicationContext()))
            mAuth.removeAuthStateListener(mAuthListener);
    }
}
