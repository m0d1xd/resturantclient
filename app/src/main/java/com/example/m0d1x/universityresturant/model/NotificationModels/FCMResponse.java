package com.example.m0d1x.universityresturant.model.NotificationModels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.xml.transform.Result;

//FireBase Cloud Messaging Response Class

public class FCMResponse {
    @SerializedName("multicast_id")
    public long multicast_id;

    @SerializedName("success")
    public int success;

    @SerializedName("failure")
    public int failure;


    @SerializedName("canonical_ids")
    public int canonical_ids;


    public List<Result> results;


    @Override
    public String toString() {
        return "FCCResponse{" +
                "mutilcast_id=" + multicast_id +
                ", success=" + success +
                ", failiure=" + failure +
                ", canonical_ids=" + canonical_ids +
                ", results=" + results +
                '}';
    }
}
