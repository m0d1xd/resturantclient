package com.example.m0d1x.universityresturant.Activities.Home;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.andremion.counterfab.CounterFab;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.example.m0d1x.universityresturant.Activities.Orders.CartActivity;
import com.example.m0d1x.universityresturant.Activities.Orders.OrderStatus;
import com.example.m0d1x.universityresturant.Activities.UserPanel.MyFavorites;
import com.example.m0d1x.universityresturant.Common.Common;
import com.example.m0d1x.universityresturant.Database.Database;
import com.example.m0d1x.universityresturant.Interface.ItemClickListener;
import com.example.m0d1x.universityresturant.MainActivity;
import com.example.m0d1x.universityresturant.R;
import com.example.m0d1x.universityresturant.ViewHolder.CategoryViewHolder;
import com.example.m0d1x.universityresturant.model.Banner;
import com.example.m0d1x.universityresturant.model.Category;
import com.example.m0d1x.universityresturant.model.NotificationModels.Token;
import com.firebase.ui.database.FirebaseRecyclerAdapter;

import android.view.LayoutInflater;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Objects;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView name, email;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference mDatabaseReference;


    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private CounterFab fab;
    private SwipeRefreshLayout swipeRefreshLayout;
    FirebaseRecyclerAdapter<Category, CategoryViewHolder> adapter;
    HashMap<String, String> image_list;
    SliderLayout slider;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            //init Slider
            slider = findViewById(R.id.slider);
            recyclerView = findViewById(R.id.recycle_menu);
            initSwipeRefreshLayout();
            initFireBase();
            //init firebase
            initAdapter();
            fab = findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    Intent cart = new Intent(HomeActivity.this, CartActivity.class);
                    startActivity(cart);
                }
            });
            fab.setCount(new Database(this).getCartCount());
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            NavigationView navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);

            View headerView = navigationView.getHeaderView(0);

            name = headerView.findViewById(R.id.tv_UserName);
            email = headerView.findViewById(R.id.tv_email);
            name.setText(Common.CurrentUser.getName());
            email.setText(Common.CurrentUser.getEmail());
        }

        //Loading Category from Firebase
        LoadCategories();
        //Token Update
        UpdateToken(FirebaseInstanceId.getInstance().getToken());
        //Setup Banner
        BannerStart();
    }

    private void initAdapter() {
        FirebaseRecyclerOptions<Category> categoryFirebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<Category>()
                .setQuery(mDatabaseReference, Category.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<Category, CategoryViewHolder>(categoryFirebaseRecyclerOptions) {
            @Override
            protected void onBindViewHolder(@NonNull CategoryViewHolder viewHolder, int i, @NonNull Category model) {
                Picasso.with(getBaseContext())
                        .load(model.getImage())
                        .into(viewHolder.img_category);
                viewHolder.tv_category_name.setText(model.getName());

                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent ItemsList = new Intent(HomeActivity.this, FoodList.class);
                        ItemsList.putExtra("menuId", adapter.getRef(position).getKey());
                        startActivity(ItemsList);
                    }
                });
            }

            @NonNull
            @Override
            public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.custon_category_menu, parent, false);

                return new CategoryViewHolder(itemView);
            }
        };
        adapter.startListening();


    }

    private void initFireBase() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = firebaseDatabase.getReference("Category");

    }

    private void initSwipeRefreshLayout() {
        swipeRefreshLayout = findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onRefresh() {
                LoadCategories();
            }
        });

        //Default load for swipeRefreshLayout
        swipeRefreshLayout.post(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void run() {
                LoadCategories();
            }
        });

    }

    private void BannerStart() {
        final DatabaseReference banner = FirebaseDatabase.getInstance().getReference("Banner");
        banner.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                image_list = new HashMap<>();
                for (DataSnapshot post : dataSnapshot.getChildren()) {
                    Banner baner = post.getValue(Banner.class);
                    image_list.put(baner.getName() + "@@@" + baner.getId(), baner.getImage());
                    for (String key : image_list.keySet()) {
                        String[] keySplit = key.split("@@@");
                        String nameOfFood = keySplit[0];
                        String idOfFood = keySplit[1];
                        final TextSliderView textSliderView = new TextSliderView(HomeActivity.this);

                        textSliderView
                                .description(nameOfFood)
                                .image(image_list.get(key))
                                .setScaleType(BaseSliderView.ScaleType.Fit)
                                .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {
                                        Intent intent = new Intent(HomeActivity.this, FoodDetails.class);
                                        intent.putExtras(textSliderView.getBundle());
                                        startActivity(intent);
                                    }
                                });

                        textSliderView.bundle(new Bundle());
                        textSliderView.getBundle().putString("foodID", idOfFood);

                        slider.addSlider(textSliderView);

                        //removing listener
                        banner.removeEventListener(this);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        slider.setPresetTransformer(SliderLayout.Transformer.Background2Foreground);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(4000);

    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
        //   slider.stopAutoCycle();
    }

    private void UpdateToken(String token) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference tokens = db.getReference("Tokens");
        Token tokenData = new Token(token, false);
        tokens.child(Common.CurrentUser.getUid()).setValue(tokenData);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void LoadCategories() {

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        //Animation
        recyclerView.scheduleLayoutAnimation();
        Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        fab.setCount(new Database(this).getCartCount());
        adapter.startListening();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            LoadCategories();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_Orders) {
            Intent intent = new Intent(HomeActivity.this, OrderStatus.class);
            startActivity(intent);
        } else if (id == R.id.nav_cart) {
            Intent intent = new Intent(HomeActivity.this, CartActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_fav) {
            Intent intent = new Intent(HomeActivity.this, MyFavorites.class);

            startActivity(intent);
        } else if (id == R.id.nav_Location) {
            //   Intent intent = new Intent(HomeActivity.this, PickDropLocation.class);
            //    startActivity(intent);
        } else if (id == R.id.nav_LogOut) {
            SignOut();

        } else if (id == R.id.nav_home_address) {

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    private void ShowHomeAddressDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);
        alertDialog.setTitle("Change Home Address");
        alertDialog.setMessage("Please fill all information");

        LayoutInflater inflater = LayoutInflater.from(this);
        View layout_home_address = inflater.inflate(R.layout.home_address_layout, null);
        final EditText edtHomeAddress = (TextInputEditText) layout_home_address.findViewById(R.id.et_home_address);

        alertDialog.setView(layout_home_address);
        alertDialog.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Common.CurrentUser.setHomeAddress(edtHomeAddress.getText().toString());

                FirebaseDatabase.getInstance().getReference("Users").child(Common.CurrentUser.getUid()).setValue(Common.CurrentUser).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(HomeActivity.this, "User Home Address Updated", Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });


    }

    public void SignOut() {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(HomeActivity.this, MainActivity.class));
        Toast.makeText(getApplicationContext(), "Good Bye", Toast.LENGTH_SHORT).show();
        finish();
    }
}
