package com.example.m0d1x.universityresturant.ViewHolder;

import android.view.View;
import android.widget.TextView;


import com.example.m0d1x.universityresturant.Interface.ItemClickListener;
import com.example.m0d1x.universityresturant.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView tv_OrderID, tv_OrderStatus, tv_Order_Phone, tv_Order_Address, tv_order_Date;

    private ItemClickListener itemClickListener;

    public OrderViewHolder(@NonNull View itemView) {
        super(itemView);

        tv_OrderID = itemView.findViewById(R.id.order_id);
        tv_OrderStatus = itemView.findViewById(R.id.order_status);
        tv_Order_Phone = itemView.findViewById(R.id.order_phone);
        tv_Order_Address = itemView.findViewById(R.id.order_Address);
        tv_order_Date = itemView.findViewById(R.id.order_Date);

        itemView.setOnClickListener(this);
    }


    public void setItemClickListener(ItemClickListener itemClickListener) {

        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }
}
