package com.example.m0d1x.universityresturant.model.NotificationModels;

import com.google.gson.annotations.SerializedName;

public class Token {


    @SerializedName("token")
    private String token;

    @SerializedName("isServerToken")
    private Boolean isServerToken;

    public Token() {
    }

    public Token(String token, Boolean isServerToken) {
        this.token = token;
        this.isServerToken = isServerToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getServerToken() {
        return isServerToken;
    }

    public void setServerToken(Boolean serverToken) {
        isServerToken = serverToken;
    }
}
