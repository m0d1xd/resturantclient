package com.example.m0d1x.universityresturant.ViewHolder;

import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.m0d1x.universityresturant.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CommentsViewHolder extends RecyclerView.ViewHolder {

    public TextView tv_user, tv_comment, tv_datetime;
    public RatingBar ratingBar;

    public CommentsViewHolder(@NonNull View itemView) {
        super(itemView);
        tv_user = itemView.findViewById(R.id.tv_User_Name);
        tv_comment = itemView.findViewById(R.id.tv_User_Comment);
        ratingBar = itemView.findViewById(R.id.rating_bar_user);
      //  tv_datetime = (TextView) itemView.findViewById(R.id.tv_date);
    }
}
