package com.example.m0d1x.universityresturant.Activities.Home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.m0d1x.universityresturant.Common.Common;
import com.example.m0d1x.universityresturant.R;
import com.example.m0d1x.universityresturant.ViewHolder.CommentsViewHolder;
import com.example.m0d1x.universityresturant.model.Rating;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class CommentsActivity extends AppCompatActivity {

    String foodId = "";

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference ratingTable;
    SwipeRefreshLayout mSwipeRefreshLayout;

    FirebaseRecyclerAdapter<Rating, CommentsViewHolder> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        //init firebase
        database = FirebaseDatabase.getInstance();
        ratingTable = database.getReference("Ratings");


        //init recycle view
        recyclerView = findViewById(R.id.recycle_comments);
        layoutManager = new LinearLayoutManager(CommentsActivity.this);

        recyclerView.setLayoutManager(layoutManager);

        Log.d("TAG", "onCreate: " + getIntent().getStringExtra(Common.INTENT_FOOD_ID));
        mSwipeRefreshLayout = findViewById(R.id.swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark
        );

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (getIntent() != null) {
                    foodId = getIntent().getStringExtra(Common.INTENT_FOOD_ID);
                    loadFoodComments(foodId);
                }
            }
        });
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                if (getIntent() != null) {
                    foodId = getIntent().getStringExtra(Common.INTENT_FOOD_ID);
                    loadFoodComments(foodId);
                }
            }
        });

    }

    private void loadFoodComments(String foodId) {

        Query query = ratingTable.orderByChild("foodId").equalTo(foodId);


        FirebaseRecyclerOptions<Rating> options = new FirebaseRecyclerOptions.Builder<Rating>()
                .setQuery(query, Rating.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<Rating, CommentsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull CommentsViewHolder commentsViewHolder, int i, @NonNull Rating rating) {
                commentsViewHolder.ratingBar.setRating(Float.parseFloat(rating.getRateValue()));
                commentsViewHolder.tv_comment.setText(rating.getComment());
                commentsViewHolder.tv_user.setText(rating.getUserName());
            }

            @NonNull
            @Override
            public CommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comments_layout, parent, false);

                return new CommentsViewHolder(view);
            }
        };

        adapter.startListening();
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        mSwipeRefreshLayout.setRefreshing(false);
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (adapter != null) {
            adapter.stopListening();
        }
    }
}
