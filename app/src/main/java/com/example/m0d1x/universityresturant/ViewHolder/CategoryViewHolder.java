package com.example.m0d1x.universityresturant.ViewHolder;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.m0d1x.universityresturant.Interface.ItemClickListener;
import com.example.m0d1x.universityresturant.R;

import androidx.recyclerview.widget.RecyclerView;


public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView tv_category_name;
    public ImageView img_category;
    private ItemClickListener itemClickListener;


    public CategoryViewHolder(View itemView) {
        super(itemView);
        tv_category_name = itemView.findViewById(R.id.tv_categoryName);
        img_category = itemView.findViewById(R.id.img_category);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }
}
