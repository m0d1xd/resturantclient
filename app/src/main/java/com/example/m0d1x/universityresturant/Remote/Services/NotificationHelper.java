package com.example.m0d1x.universityresturant.Remote.Services;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;

import com.example.m0d1x.universityresturant.R;


public class NotificationHelper extends ContextWrapper {
    private static final String ARCHIVO_CHANNLE_ID = "com.example.m0d1x.universityresturant";
    private static final String ARCHIVO_CHANNLE_NAME = "Resturant";
    private NotificationManager manager;

    public NotificationHelper(Context base) {
        super(base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            createChannel();

    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel() {

        NotificationChannel notificationChannel = new NotificationChannel(ARCHIVO_CHANNLE_ID,
                ARCHIVO_CHANNLE_NAME,
                NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.setDescription("");
        notificationChannel.enableLights(true);
        notificationChannel.enableVibration(true);
        notificationChannel.setLightColor(Color.BLUE);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);


        getManager().createNotificationChannel(notificationChannel);

    }


    public NotificationManager getManager() {
        if (manager == null)
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        return manager;
    }


    @TargetApi(Build.VERSION_CODES.O)
    public Notification.Builder getResturantChannleNotification(String title, String body, PendingIntent conent, Uri soundUri) {
        return new Notification.Builder(getApplicationContext(), ARCHIVO_CHANNLE_ID)
                .setContentIntent(conent)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.icon_burger)
                .setSound(soundUri)
                .setAutoCancel(false);

    }
}
