package com.example.m0d1x.universityresturant.ViewHolder;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.m0d1x.universityresturant.Activities.Orders.CartActivity;
import com.example.m0d1x.universityresturant.Common.Common;
import com.example.m0d1x.universityresturant.Database.Database;
import com.example.m0d1x.universityresturant.Interface.ItemClickListener;
import com.example.m0d1x.universityresturant.R;
import com.example.m0d1x.universityresturant.model.Order;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

class CartViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
        View.OnCreateContextMenuListener {


    public TextView tv_itemName, tv_itemPrice;
    public ImageView img_item;
    public ElegantNumberButton quantity;

    private ItemClickListener itemClickListener;

    public void setTv_itemName(TextView tv_itemName) {
        this.tv_itemName = tv_itemName;

    }

    public CartViewHolder(View itemView) {
        super(itemView);
        tv_itemName = itemView.findViewById(R.id.order_itemName);
        tv_itemPrice = itemView.findViewById(R.id.order_itemPrice);
        img_item = itemView.findViewById(R.id.order_image);
        quantity = itemView.findViewById(R.id.food_counter_quantity);

        itemView.setOnCreateContextMenuListener(this);
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.setHeaderTitle("Select Action");
        contextMenu.add(0, 0, getAdapterPosition(), Common.DELETE);

    }
}

public class CartAdapter extends RecyclerView.Adapter<CartViewHolder> {

    List<Order> listData = new ArrayList<>();
    CartActivity cart;

    public CartAdapter(List<Order> listData, CartActivity cart) {
        this.listData = listData;
        this.cart = cart;

    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(cart);
        View itemView = inflater.inflate(R.layout.cart_layout_new, parent, false);


        return new CartViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, final int position) {
        Locale local = new Locale("en", "US");
        Picasso.with(cart.getBaseContext())
                .load(listData.get(position).getImage())
                .resize(70, 70)
                .centerCrop()
                .into(holder.img_item);
        NumberFormat fmt = NumberFormat.getCurrencyInstance(local);

        holder.quantity.setNumber(listData.get(position).getQuantity());

        holder.quantity.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                Order order = listData.get(position);
                order.setQuantity(String.valueOf(newValue));
                new Database(cart).UpdateCart(order);
                Updateprice();
            }
        });

        int totalp = (Integer.parseInt(listData.get(position).getPrice())) * Integer.parseInt(listData.get(position).getQuantity());

        int total = Integer.parseInt(listData.get(position).getPrice());

        holder.tv_itemPrice.setText(fmt.format(total) + "\t* " + listData.get(position).getQuantity() + "\t = " + fmt.format(totalp));

        holder.tv_itemName.setText(listData.get(position).getProductName());

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public void Updateprice() {
        int total = 0;
        List<Order> orders = new Database(cart).getOrders();

        for (Order order : orders)
            total += (Integer.parseInt(order.getQuantity()) * Integer.parseInt(order.getPrice()));


        Locale local = new Locale("he", "he_IL");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(local);


        cart.tv_totalPrice.setText(String.valueOf(fmt.format(total)));
        this.notifyDataSetChanged();

    }
}
