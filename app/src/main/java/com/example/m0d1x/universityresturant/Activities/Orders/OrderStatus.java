package com.example.m0d1x.universityresturant.Activities.Orders;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.m0d1x.universityresturant.Common.Common;
import com.example.m0d1x.universityresturant.Interface.ItemClickListener;
import com.example.m0d1x.universityresturant.R;
import com.example.m0d1x.universityresturant.ViewHolder.OrderViewHolder;
import com.example.m0d1x.universityresturant.model.Request;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class OrderStatus extends AppCompatActivity {


    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    FirebaseRecyclerAdapter<Request, OrderViewHolder> adapter;

    FirebaseDatabase database;
    DatabaseReference requests;


    TextView tv_totalPrice;
    Button btn_placeOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);


        //Init Firebase
        database = FirebaseDatabase.getInstance();
        requests = database.getReference("Requests");

        recyclerView = findViewById(R.id.listOrders);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        LoadOrders(Common.CurrentUser.getUid());

        

    }

    private void LoadOrders(String uid) {
        Query query = requests.orderByChild("uid").equalTo(uid);

        FirebaseRecyclerOptions<Request> orderOptions = new FirebaseRecyclerOptions.Builder<Request>().setQuery(query, Request.class).build();

        adapter = new FirebaseRecyclerAdapter<Request, OrderViewHolder>(orderOptions) {
            @Override
            protected void onBindViewHolder(@NonNull OrderViewHolder orderViewHolder, int position, @NonNull final Request model) {
                orderViewHolder.tv_OrderID.setText(adapter.getRef(position).getKey());
                orderViewHolder.tv_OrderStatus.setText(ConvertCodeToStatus(model.getStatus()));
                orderViewHolder.tv_Order_Address.setText(model.getAddress());
                orderViewHolder.tv_Order_Phone.setText(model.getName());
                orderViewHolder.tv_order_Date.setText(DateFormat.format("dd-MM-yyyy       TIME hh:mm", Long.parseLong(adapter.getRef(position).getKey())));

                orderViewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Log.d("DEBUG mo", "onClick: look activity");
                        //  Intent Tracking = new Intent(OrderStatus.this, TrackingActivity.class);
                        //  Common.CurrentRequest = model;
                        //    startActivity(Tracking);

                    }
                });
            }

            @NonNull
            @Override
            public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_layout, parent, false);


                return new OrderViewHolder(itemView);
            }
        };
        adapter.startListening();
        recyclerView.setAdapter(adapter);
    }

    private String ConvertCodeToStatus(String status) {
        switch (status) {
            case "0":
                return "Placed";
            case "1":
                return "On The Way ! ";
            case "2":
                return "Shipped";
            default:
                return "Unknown";
        }

    }
}
