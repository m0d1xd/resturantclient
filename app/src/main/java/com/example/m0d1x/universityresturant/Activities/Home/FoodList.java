package com.example.m0d1x.universityresturant.Activities.Home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.m0d1x.universityresturant.Database.Database;
import com.example.m0d1x.universityresturant.Interface.ItemClickListener;
import com.example.m0d1x.universityresturant.R;
import com.example.m0d1x.universityresturant.ViewHolder.FoodViewHolder;
import com.example.m0d1x.universityresturant.model.Favourites;
import com.example.m0d1x.universityresturant.model.Food;
import com.example.m0d1x.universityresturant.model.Order;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FoodList extends AppCompatActivity {


    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FirebaseDatabase database;
    private DatabaseReference FoodList;

    private String CatID = "";
    private FirebaseRecyclerAdapter<Food, FoodViewHolder> adapter;

    //Search Functionallity
    private FirebaseRecyclerAdapter<Food, FoodViewHolder> SearchAdapter;
    private List<String> Suggestion = new ArrayList<>();
    private MaterialSearchBar searchBar;
    Database fav_db;

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);

        initSRL();
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                //Getting User last sellection For Catagory
                if (getIntent() != null) {
                    CatID = getIntent().getStringExtra("menuId");
                }
                if (!CatID.isEmpty()) {
                    LoadItems(CatID);
                }

                //Init Views
                searchBar = findViewById(R.id.searchBar);
                searchBar.setHint("Search Food");

                LoadSuggestions();

                searchBar.setCardViewElevation(10);
                searchBar.addTextChangeListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }
                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        List<String> Suggest = new ArrayList<>();
                        for (String search : Suggestion) {
                            if (search.toLowerCase().contains(searchBar.getText().toLowerCase())) {
                                Suggest.add(search);
                            }
                            searchBar.setLastSuggestions(Suggest);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
                searchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
                    @Override
                    public void onSearchStateChanged(boolean enabled) {
                        //when action bar is closed
                        //retrieve all list
                        if (!enabled) {
                            recyclerView.setAdapter(adapter);
                        }
                    }

                    @Override
                    public void onSearchConfirmed(CharSequence text) {
                        searchFood(text);
                    }

                    @Override
                    public void onButtonClicked(int buttonCode) {

                    }
                });
            }
        });

        //Local Favourite Database;
        fav_db = new Database(this);

        //Init FirebaseDB
        database = FirebaseDatabase.getInstance();
        FoodList = database.getReference("Foods");

        recyclerView = findViewById(R.id.recycle_food_menu);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


    }

    private void initSRL() {
        swipeRefreshLayout = findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark
        );
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Getting User last sellection For Catagory
                if (getIntent() != null) {
                    CatID = getIntent().getStringExtra("menuId");
                }
                if (!CatID.isEmpty()) {
                    LoadItems(CatID);
                }
            }
        });

    }

    //TODO Fix Search food
    private void searchFood(CharSequence text) {
        Query query = FoodList.orderByChild("name").equalTo(text.toString());
        FirebaseRecyclerOptions<Food> foodOptions = new FirebaseRecyclerOptions.Builder<Food>()
                .setQuery(query, Food.class)
                .build();

        SearchAdapter = new FirebaseRecyclerAdapter<Food, FoodViewHolder>(foodOptions) {
            @Override
            protected void onBindViewHolder(@NonNull FoodViewHolder foodViewHolder, int i, @NonNull Food food) {
                foodViewHolder.tv_food_name.setText(food.getName());
                Picasso.with(getBaseContext()).load(food.getImage()).into(foodViewHolder.img_food);
                final Food clickitem = food;
                foodViewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                        Intent itemDetails = new Intent(FoodList.this, FoodDetails.class);
                        itemDetails.putExtra("foodID", SearchAdapter.getRef(position).getKey());
                        startActivity(itemDetails);

                    }
                });
            }

            @NonNull
            @Override
            public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.food_list, parent, false);

                return new FoodViewHolder(itemView);
            }
        };
        recyclerView.setAdapter(SearchAdapter);
        SearchAdapter.startListening();
        SearchAdapter.notifyDataSetChanged();
    }

    private void LoadSuggestions() {

        searchBar.setLastSuggestions(Suggestion);
        FoodList.orderByChild("menuId").equalTo(CatID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            Food item = postSnapshot.getValue(Food.class);
                            Suggestion.add(item.getName());
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


    }

    private void LoadItems(String catID) {

        Query query = FoodList.orderByChild("menuId").equalTo(catID);

        FirebaseRecyclerOptions<Food> foodFireballRecyclerOptions = new FirebaseRecyclerOptions.Builder<Food>()
                .setQuery(query, Food.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<Food, FoodViewHolder>(foodFireballRecyclerOptions) {
            @Override
            protected void onBindViewHolder(@NonNull final FoodViewHolder viewHolder, final int position, @NonNull final Food model) {
                viewHolder.tv_food_name.setText(model.getName());
                //     viewHolder.tv_food_price.setText(String.format("$ %s", model.getPrice()));
                Picasso.with(getBaseContext()).load(model.getImage()).into(viewHolder.img_food);
                if (fav_db.isFavorite(adapter.getRef(position).getKey())) {
                    viewHolder.img_fav.setImageResource(R.drawable.ic_favorite_black_24dp);
                }
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                        Intent itemDetails = new Intent(FoodList.this, FoodDetails.class);
                        itemDetails.putExtra("foodID", adapter.getRef(position).getKey());
                        startActivity(itemDetails);

                    }
                });

                viewHolder.img_fav.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Favourites favourites = new Favourites();
                        favourites.setFoodId(adapter.getRef(position).getKey());
                        favourites.setFoodName(model.getName());
                        favourites.setFoodImage(model.getImage());
                        favourites.setFoodDescription(model.getDescription());
                        favourites.setFoodDiscount(model.getDiscount());
                        favourites.setFoodMenuId(model.getMenuId());
                        favourites.setFoodPrice(model.getPrice());


                        if (!fav_db.isFavorite(favourites.getFoodId())) {
                            fav_db.AddToFavourites(favourites);
                            viewHolder.img_fav.setImageResource(R.drawable.ic_favorite_black_24dp);
                            Toast.makeText(FoodList.this, model.getName() + " Added To Favourites", Toast.LENGTH_SHORT).show();
                        } else {
                            fav_db.RemoveFromFavorites(favourites.getFoodId());
                            viewHolder.img_fav.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                            Toast.makeText(FoodList.this, model.getName() + " Removed From Favourites", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
                viewHolder.img_addToCart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new Database(getBaseContext()).addToCart(new Order(
                                adapter.getRef(position).getKey(),
                                model.getName(),
                                "" + 1,
                                model.getPrice(),
                                model.getDiscount(),
                                model.getImage()
                        ));

                        Toast.makeText(getApplicationContext(), "Added To Cart", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @NonNull
            @Override
            public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.food_list_layout, parent, false);

                return new FoodViewHolder(itemView);
            }
        };

        adapter.startListening();
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);

    }
}
