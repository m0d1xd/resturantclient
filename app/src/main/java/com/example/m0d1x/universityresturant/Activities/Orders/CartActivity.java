package com.example.m0d1x.universityresturant.Activities.Orders;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.m0d1x.universityresturant.Common.Common;
import com.example.m0d1x.universityresturant.Database.Database;
import com.example.m0d1x.universityresturant.R;
import com.example.m0d1x.universityresturant.Remote.APIServices;
import com.example.m0d1x.universityresturant.Remote.IGoogleService;
import com.example.m0d1x.universityresturant.ViewHolder.CartAdapter;
import com.example.m0d1x.universityresturant.model.NotificationModels.FCMResponse;
import com.example.m0d1x.universityresturant.model.NotificationModels.Notification;
import com.example.m0d1x.universityresturant.model.Order;
import com.example.m0d1x.universityresturant.model.Request;
import com.example.m0d1x.universityresturant.model.NotificationModels.Sender;
import com.example.m0d1x.universityresturant.model.NotificationModels.Token;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CartActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private FirebaseDatabase database;
    private DatabaseReference requests;
    int PLACE_PICKER_REQUEST = 1;
    public TextView tv_totalPrice;
    private List<Order> cart = new ArrayList<>();

    private APIServices mServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        //Init API services
        mServices = Common.getFCMService();
        //Init Firebases
        database = FirebaseDatabase.getInstance();
        requests = database.getReference("Requests");
        recyclerView = findViewById(R.id.listCart);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        tv_totalPrice = findViewById(R.id.tv_total);
        Button btn_placeOrder = findViewById(R.id.btn_PlaceOrder);
        btn_placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cart.size() > 0) {
                    showAlertDialog();
                } else {
                    Toast.makeText(CartActivity.this, "Your Cart Is Empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

        LoadListFood();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(CartActivity.this, data);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if (item.getTitle().equals(Common.DELETE)) {
            DeleteCart(item.getOrder());
        }
        return true;
    }

    private void DeleteCart(int position) {
        cart.remove(position);
        new Database(this).clearCart();
        for (Order item : cart) {
            new Database(this).addToCart(item);
        }
        LoadListFood();
    }

    private void showAlertDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(CartActivity.this);
        alertDialog.setTitle("One more Step");
        alertDialog.setMessage("Enter Your Adress");
        LayoutInflater inflater = this.getLayoutInflater();
        final View orderAddressComment = inflater.inflate(R.layout.order_comment, null);

        final MaterialEditText edtAddress = orderAddressComment.findViewById(R.id.et_Address);


        //RadioGroup
        RadioButton shipToAddress = orderAddressComment.findViewById(R.id.rdiShipToAddress);
        RadioButton shipToHome = orderAddressComment.findViewById(R.id.rdiShipToHome);

        shipToAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //Ship to this address feature
                if (isChecked) {

                }

            }
        });


        ImageView choseOnMap = orderAddressComment.findViewById(R.id.img_choseOnMap);
        choseOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                Intent intent;
                try {
                    intent = builder.build((Activity) getApplicationContext());
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesNotAvailableException e) {
                } catch (GooglePlayServicesRepairableException e) {
                }
            }
        });


        final MaterialEditText edtComment = orderAddressComment.findViewById(R.id.et_comment);


        alertDialog.setView(orderAddressComment);
        alertDialog.setIcon(R.drawable.ic_shopping_cart_black_24dp);
        final String address;

        //address = shippingAddress.getAddress().toString();
        address = edtAddress.getText().toString();
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Request request = new Request(
                        Common.CurrentUser.getUid(),
                        Common.CurrentUser.getName(),
                        address,
                        tv_totalPrice.getText().toString(),
                        cart,
                        edtComment.getText().toString(),
                        "0");

                String Current_order = String.valueOf(System.currentTimeMillis());

                requests.child(Current_order).setValue(request);

                //Clear cart

                sendNotification(Current_order);

                new Database(getBaseContext()).clearCart();
                Toast.makeText(CartActivity.this, "Thank You for your Order", Toast.LENGTH_SHORT).show();

                finish();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();

    }

    private void sendNotification(final String current_order) {
        Log.d("", "sendNotification: ");
        DatabaseReference tokens = database.getReference("Tokens");
        Query data = tokens.orderByChild("serverToken").equalTo("true");
        data.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postDatasnapShot : dataSnapshot.getChildren()) {
                    Token serverToken = postDatasnapShot.getValue(Token.class);
                    Notification notification = new Notification("Resturant ", "You have new Order " + current_order);
                    Sender content = new Sender(serverToken.getToken(), notification);
                    Log.d("TOKENTEST", "onDataChange: " + content.getTo());
                    mServices.sendNotification(content).enqueue(new Callback<FCMResponse>() {
                        @Override
                        public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                            if (response.code() == 200) {
                                Log.d("mServices", "onResponse: " + response.code());
                                if (response.body().success == 1) {
                                    Toast.makeText(getApplicationContext(), "Thanks for ordering", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(CartActivity.this, "Faild Sending order", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Log.d("mServices", "onResponse: " + response.code());
                            }
                        }

                        @Override
                        public void onFailure(Call<FCMResponse> call, Throwable t) {
                            Log.e("ERROR", "onFailure: " + t.getMessage());
                        }
                    });


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void LoadListFood() {
        cart = new Database(this).getOrders();

        CartAdapter adapter = new CartAdapter(cart, this);
        adapter.notifyDataSetChanged();

        recyclerView.setAdapter(adapter);

        int total = 0;

        List<Order> orders = new Database(this).getOrders();
        for (Order order : orders)
            total += (Integer.parseInt(order.getQuantity()) * Integer.parseInt(order.getPrice()));

        Locale local = new Locale("he", "he_IL");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(local);


        tv_totalPrice.setText(String.valueOf(fmt.format(total)));
        adapter.notifyDataSetChanged();

    }

}

//TODO method to check if user is in the resturant or not ,
//TODO Later adding QR code scanner to set the Address and location of the user indicating that he is in the resturant
//TODO Adding points after every purchase for the user



