package com.example.m0d1x.universityresturant.model;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class Request {
    private String name, Phone, address, total, status, uid, comment;
    private GeoLocation geoLocation;
    private List<Order> foods;
    private GeoFire location;
    private LatLng coordinate;

    public Request() {
    }


    public Request(String name, String phone, String address, String total, String status, String uid, String comment, List<Order> foods, LatLng coordinate) {
        this.name = name;
        Phone = phone;
        this.address = address;
        this.total = total;
        this.status = status;
        this.uid = uid;
        this.comment = comment;
        this.foods = foods;
        this.coordinate = coordinate;
    }


    public Request(String uid, String name, String address, String total, List<Order> foods, String comment, String status, LatLng latLng) {
        this.name = name;
        this.uid = uid;
        this.address = address;
        this.total = total;
        this.comment = comment;
        this.foods = foods;
        this.status = status;
        this.coordinate = latLng;
    }

    public Request(String uid, String name, String address, String total, List<Order> foods, String comment, String status) {
        this.name = name;
        this.uid = uid;
        this.address = address;
        this.total = total;
        this.comment = comment;
        this.foods = foods;
        this.status = status;
    }

    public Request(String uid, String name, String total, List<Order> foods, String status, GeoLocation geoLocation) {
        this.name = name;
        this.uid = uid;
        this.total = total;
        this.foods = foods;
        this.status = status;
        this.geoLocation = geoLocation;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public List<Order> getFoods() {
        return foods;
    }

    public void setFoods(List<Order> foods) {
        this.foods = foods;
    }

    public GeoFire getLocation() {
        return location;
    }

    public void setLocation(GeoFire location) {
        this.location = location;
    }

    public LatLng getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(LatLng coordinate) {
        this.coordinate = coordinate;
    }
}
