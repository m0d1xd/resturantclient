package com.example.m0d1x.universityresturant.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import com.example.m0d1x.universityresturant.model.Favourites;
import com.example.m0d1x.universityresturant.model.Order;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteAssetHelper {

    static private final String DB_Name = "eatit.db";
    static private final int DB_VER = 3;

    //
    private static final String TABLE_ORDER_DETAILS = "OrderDetails";
    private static final String TABLE_FAVOURITES = "Favorites";
    private static final String TAG = "";

    // OrderDetails Table Columns
    private static final String KEY_ORDER_ID = "OrderID";
    private static final String KEY_ORDER_ProductID = "ProductID";
    private static final String KEY_ORDER_PRODUCT_NAME = "ProductName";
    private static final String KEY_ORDER_QUANTITY = "Quantity";
    private static final String KEY_ORDER_PRICE = "Price";
    private static final String KEY_ORDER_DISCOUNT = "Discount";
    private static final String KEY_ORDER_IMAGE_URL = "Image";

    //Favorites Table Columns
    private static final String KEY_FAVOURITES_FOOD_ID = "FoodId";
    private static final String KEY_FAVOURITES_FOOD_NAME = "foodName";
    private static final String KEY_FAVOURITES_FOOD_PRICE = "foodPrice";
    private static final String KEY_FAVOURITES_FOOD_DISCOUNT = "foodDiscount";
    private static final String KEY_FAVOURITES_FOOD_DESCRIPTION = "foodDescription";
    private static final String KEY_FAVOURITES_FOOD_MENU_ID = "foodMenuId";
    private static final String KEY_FAVOURITES_FOOD_IMAGE_URL = "foodImage";


    public Database(Context context) {
        super(context, DB_Name, null, DB_VER);
    }

    public void addToCart(Order order) {

        //Create and/or open the database for writing
        SQLiteDatabase db = getReadableDatabase();
        db.beginTransaction();


        // wrap our insert in a transaction. This helps with performance and ensures
        // consistency of the database.
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_ORDER_ProductID, order.getProductID());
            values.put(KEY_ORDER_PRODUCT_NAME, order.getProductName());
            values.put(KEY_ORDER_QUANTITY, order.getQuantity());
            values.put(KEY_ORDER_PRICE, order.getPrice());
            values.put(KEY_ORDER_DISCOUNT, order.getDiscount());
            values.put(KEY_ORDER_IMAGE_URL, order.getImage());

            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
            db.insertOrThrow(TABLE_ORDER_DETAILS, null, values);

            db.setTransactionSuccessful();
            //TRANSACTION COMPLETED
            Log.d(TAG, "addToCart: TRANSACTION COMPLETED " + db.isOpen());
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add Order to database");
        } finally {
            db.endTransaction();
            db.close();
            Log.d(TAG, "addToCart: TRANSACTION COMPLETED " + db.isOpen());
        }

    }

    public void clearCart() {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM OrderDetails");
        db.execSQL(query);
    }

    public List<Order> getOrders() {

        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String[] sqlSelect = {
                KEY_ORDER_ID,
                KEY_ORDER_ProductID,
                KEY_ORDER_PRODUCT_NAME,
                KEY_ORDER_QUANTITY,
                KEY_ORDER_PRICE,
                KEY_ORDER_DISCOUNT,
                KEY_ORDER_IMAGE_URL
        };

        String sqlTable = TABLE_ORDER_DETAILS;


        qb.setTables(sqlTable);
        Cursor c = qb.query(db, sqlSelect, null, null, null, null, null);
        final List<Order> result = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                // Order(String productID, String productName, String quantity, String price, String discount, String ImageUrl)
                result.add(new Order(
                                c.getInt(c.getColumnIndex(KEY_ORDER_ID)),
                                c.getString(c.getColumnIndex(KEY_ORDER_ProductID)),
                                c.getString(c.getColumnIndex(KEY_ORDER_PRODUCT_NAME)),
                                c.getString(c.getColumnIndex(KEY_ORDER_QUANTITY)),
                                c.getString(c.getColumnIndex(KEY_ORDER_PRICE)),
                                c.getString(c.getColumnIndex(KEY_ORDER_DISCOUNT)),
                                c.getString(c.getColumnIndex(KEY_ORDER_IMAGE_URL))
                        )
                );
            } while (c.moveToNext());
        }

        return result;
    }

    public void AddToFavourites(Favourites food) {
        //Create and/or open the database for writing
        SQLiteDatabase db = getReadableDatabase();
        db.beginTransaction();

        try {
            ContentValues values = new ContentValues();
            values.put(KEY_FAVOURITES_FOOD_ID, food.getFoodId());
            values.put(KEY_FAVOURITES_FOOD_NAME, food.getFoodName());
            values.put(KEY_FAVOURITES_FOOD_PRICE, food.getFoodPrice());
            values.put(KEY_FAVOURITES_FOOD_MENU_ID, food.getFoodMenuId());
            values.put(KEY_FAVOURITES_FOOD_IMAGE_URL, food.getFoodImage());
            values.put(KEY_FAVOURITES_FOOD_DESCRIPTION, food.getFoodDescription());
            values.put(KEY_FAVOURITES_FOOD_DISCOUNT, food.getFoodDiscount());

            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
            db.insertOrThrow(TABLE_FAVOURITES, null, values);

            db.setTransactionSuccessful();
            //TRANSACTION COMPLETED
            Log.d(TAG, "addToCart: TRANSACTION COMPLETED " + db.isOpen());
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add Order to database");
        } finally {
            db.endTransaction();
            db.close();
            Log.d(TAG, "addToCart: TRANSACTION COMPLETED " + db.isOpen());
        }
    }

    public void RemoveFromFavorites(String FoodId) {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM " + TABLE_FAVOURITES + " WHERE FoodID='%s';", FoodId);
        db.execSQL(query);

    }

    public boolean isFavorite(String FoodId) {

        SQLiteDatabase db = getReadableDatabase();

        String query = String.format("SELECT * FROM Favorites WHERE FoodId='%s';", FoodId);
        Cursor c = db.rawQuery(query, null);
        return c.getCount() != 0;
    }


    public int getCartCount() {
        SQLiteDatabase db = getReadableDatabase();
        int count = 0;

        String query = String.format("SELECT Count(*) FROM OrderDetails;");
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                count += c.getInt(0);
            } while (c.moveToNext());
        } else {

        }
        return count;
    }

    public void UpdateCart(Order order) {
        SQLiteDatabase db = getReadableDatabase();
        String Query = String.format("UPDATE OrderDetails SET Quantity= %s WHERE OrderID=%s ;", order.getQuantity(), order.getID());
        db.execSQL(Query);
    }

    public List<Favourites> getFavourites() {

        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String[] sqlSelect = {
                KEY_FAVOURITES_FOOD_ID,
                KEY_FAVOURITES_FOOD_NAME,
                KEY_FAVOURITES_FOOD_PRICE,
                KEY_FAVOURITES_FOOD_DISCOUNT,
                KEY_FAVOURITES_FOOD_DESCRIPTION,
                KEY_FAVOURITES_FOOD_MENU_ID,
                KEY_FAVOURITES_FOOD_IMAGE_URL
        };

        String sqlTable = TABLE_FAVOURITES;


        qb.setTables(sqlTable);

        Cursor c = qb.query(db, sqlSelect, null, null, null, null, null);
        final List<Favourites> result = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                result.add(new Favourites(
                        c.getString(c.getColumnIndex(KEY_FAVOURITES_FOOD_ID)),
                        c.getString(c.getColumnIndex(KEY_FAVOURITES_FOOD_NAME)),
                        c.getString(c.getColumnIndex(KEY_FAVOURITES_FOOD_PRICE)),
                        c.getString(c.getColumnIndex(KEY_FAVOURITES_FOOD_MENU_ID)),
                        c.getString(c.getColumnIndex(KEY_FAVOURITES_FOOD_IMAGE_URL)),
                        c.getString(c.getColumnIndex(KEY_FAVOURITES_FOOD_DESCRIPTION)),
                        c.getString(c.getColumnIndex(KEY_FAVOURITES_FOOD_DISCOUNT))
                ));
            } while (c.moveToNext());
        }

        return result;
    }


}

