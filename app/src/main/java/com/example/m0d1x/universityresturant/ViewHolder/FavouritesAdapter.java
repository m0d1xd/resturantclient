package com.example.m0d1x.universityresturant.ViewHolder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.m0d1x.universityresturant.Activities.Home.FoodList;
import com.example.m0d1x.universityresturant.Database.Database;
import com.example.m0d1x.universityresturant.Interface.ItemClickListener;
import com.example.m0d1x.universityresturant.R;
import com.example.m0d1x.universityresturant.model.Favourites;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

class FavouritesViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {


    public TextView tv_food_name;
    public ImageView img_item, img_fav, img_addToCart;

    private ItemClickListener itemClickListener;


    public FavouritesViewHolder(View itemView) {
        super(itemView);
        tv_food_name = itemView.findViewById(R.id.tv_fav_food_name);
        img_item = itemView.findViewById(R.id.img_fav_food);
        img_fav = itemView.findViewById(R.id.img_fav);
        img_addToCart = itemView.findViewById(R.id.img_fav_addToCard);

        itemView.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

    }

}

public class FavouritesAdapter extends RecyclerView.Adapter<FavouritesViewHolder> {

    List<Favourites> listData = new ArrayList<>();
    Context mContext;
    Database fav_db;

    public FavouritesAdapter(List<Favourites> favs, Context context) {
        this.listData = favs;
        this.mContext = context;
        fav_db = new Database(mContext);
    }

    @NonNull
    @Override
    public FavouritesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.custom_fav_layout, parent, false);


        return new FavouritesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouritesViewHolder holder, final int position) {

        Picasso.with(mContext).load(listData.get(position).getFoodImage())
                .resize(70, 70)
                .centerCrop()
                .into(holder.img_item);


        holder.tv_food_name.setText(listData.get(position).getFoodName());


        if (!fav_db.isFavorite(listData.get(position).getFoodId())) {
            fav_db.AddToFavourites(listData.get(position));
            holder.img_fav.setImageResource(R.drawable.ic_favorite_black_24dp);
            // Toast.makeText(this, model.getName() + " Added To Favourites", Toast.LENGTH_SHORT).show();
        } else {
            // fav_db.RemoveFromFavorites(listData.get(position).getFoodId());
            //    holder.img_fav.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        }

        holder.img_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.img_addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return listData.size();
    }
}


